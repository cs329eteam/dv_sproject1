require(tidyr)
require(dplyr)
require(ggplot2)
require(RCurl)
require(jsonlite)

c02 <- data.frame(fromJSON(getURL(URLencode('oraclerest.cs.utexas.edu:5001/rest/native/?query="select * from co2_emissions"'),httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER='cs329e_jtm3433', PASS='orcl_jtm3433', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'), verbose = TRUE) ))
summary(c02)
summary(subset(c02,COUNTRY=="United States"))






