---
title: "Project 4"
author: "John Mitchell, Matt Strasburg, John Dominguez"
date: "October 26, 2016"
output: 
  html_document: 
    toc: yes
  html_notebook: 
    toc: yes
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Session Info
```{r }
sessionInfo()
```

##ETL Operations
For this project we used a csv containing different countries CO2 emissions by year. Using the following ETL file, we were able to clean up the csv and use the created print statement to create the table in SQL Developer. Using the outputed csv, we were able to add the data to the table so we could use it in our Shiny application.

ETL Operations:
```{r}
  source("../01 Data/R_ETL.R", echo = TRUE)
```

##Summary
Here is a summary of our dataset, which is the table we pulled from the oracle database. Also included is the first few rows from a subset of the data, showing some of the United State's emissions.
```{r}
source("../01 Data/co2_summary.R", echo = TRUE)
```

##Shiny App
We had trouble embedding the Shiny app into the rmd file. Initial tries gave back a working directory conflict. We seemed to had figured it out but then having the shiny app caused source() to not work correctly, so here are screenshots of our 3 tabs in our Shiny app. 
The Shiny App can be found in 02Shiny and can be run by opening one of the ui.R or server.R files and clicking run and is published here https://matt-datavis.shinyapps.io/02shiny_app/.

###Tab 1
For the first graph, we narrowed the data down to four countries and plotted them with one another showing how they compare in emissions growth over time.
![Tab 1 in Shiny app](../01 Data/tab1.png)

###Tab 2
For our second graph in the second tab, we plotted the United State's emissions for a recent period of 1990-present to show recent trends.
![Tab 2 in Shiny app](../01 Data/tab2.png)

###Tab 3
Our third graph in the third tab takes a snapshot of the data in 2010 to compare the four contries emissions in the form of a bar graph.
![Tab 3 in Shiny app](../01 Data/tab3.png)